<?php

//Login
if ((!isset($_SERVER["PHP_AUTH_USER"])) || (!isset($_SERVER["PHP_AUTH_PW"]))
    || ($_SERVER["PHP_AUTH_USER"] != 'zig') || (md5($_SERVER["PHP_AUTH_PW"]) != 'b727b21adcf91422d8cd104712c4d3c4')
) {

    header('WWW-Authenticate: Basic realm="Private"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Authorization Required.';
    exit;
}


//Only available in ZigCMS2
if (file_exists('./typo3conf/LocalConfiguration.php')) {
    $localConf = include_once './typo3conf/LocalConfiguration.php';
}

$extPath = './typo3conf/ext';
$extensions = array();

//Genererate extensions names and versions
$objects = new RecursiveDirectoryIterator($extPath);
foreach ($objects as $name => $object) {
    if (file_exists($name . '/ext_emconf.php')) {
        $extensionName = explode('/', $name);
        $extensionName = $extensionName[count($extensionName) - 1];
        $_EXTKEY = $extensionName;
        require_once $name . '/ext_emconf.php';

        $extensions[$extensionName] = array(
            'version' => $EM_CONF[$_EXTKEY]['version'],
        );

        //Only available in ZigCMS2
        if (isset($localConf)) {
            $extensions[$extensionName]['installed'] = isset($localConf['EXT']['extConf'][$extensionName]);
        }
    }
}

//How is the site linked, direct to a version or via the latest symlink
$locationSym = readlink('typo3_src');
$linkMethod = 'direct';
if (strpos($locationSym, 'zigcms') > 0) {
    $linkMethod = 'latest';
}

//TYPO3 and ZigCMS version information
$realPath = realpath('typo3_src');
$zigCMSVersion = explode('/', $realPath);
$zigCMSVersion = $zigCMSVersion[count($zigCMSVersion) - 1];
$typo3Version = explode('/', realpath('typo3_src/typo3'));
$typo3Version = str_replace('typo3_src-', '', $typo3Version[count($typo3Version) - 2]);

//Get ZigCMS versions on the server
$availableVersions = array();
$objects = new RecursiveDirectoryIterator($realPath . '/../');
foreach ($objects as $name => $object) {
    $availableVersion = explode('/', $name);
    $availableVersion = $availableVersion[count($availableVersion) - 1];
    if ($availableVersion[0] != '.') {
        $availableVersions[] = $availableVersion;
    }
}

//Generate output
$info = array();
$info['general'] = array(
    'linkMethod'    => $linkMethod,
    'zigCMSVersion' => $zigCMSVersion,
    'typo3Version'  => $typo3Version,
);
$info['extensions'] = $extensions;
$info['availableVersions'] = $availableVersions;

echo json_encode($info);
